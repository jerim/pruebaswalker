@extends('layouts.app')
@section('content')

<div class="container" style="margin-left: 2px; margin-bottom: 2px;">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Modificar Viajes tematicos</h1>
         <form method="POST" action="/viajesTematicos/{{$viajesTematicos->cod_viajesTematicos}}" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="_method" value="PUT">


            <div class="form-group">
                <label>Titulo</label>
                <input class="form-control" type="text" name="titulo" value="{{$viajesTematicos->titulo}}">
            </div>

             <div class="form-group">
                <label>Origen</label>
                <input class="form-control" type="text" name="origenYdestino" value="{{$viajesTematicos->origenYdestino}}">
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" value="{{$viajesTematicos->descripcion}}">
            </div>

            <div class="form-group">
                <label>Numero de personas</label>
                <input class="form-control" type="Integer" name="numPersona" value="{{$viajesTematicos->numPersona}}">
            </div>


            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" value="{{$viajesTematicos->precio}}">
            </div>


            <input type="submit" class="btn btn-success" name="Modificar" value="Modificar">
            <a href="/viajesTematicos" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->


@endsection

