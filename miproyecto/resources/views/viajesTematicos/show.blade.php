@extends('layouts.app')
@section('content')
<div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-header">{{$viajesTematicos->titulo}}</div>
    </div>
    <div class="card-body">
      <p class="card-text">Sitios a visitar:{{$viajesTematicos->origenYdestino}}</p>
      <p class="card-text">Grupos de personas:{{$viajesTematicos->numPersona}}</p>
      <p class="card-text">{{$viajesTematicos->descripcion}}</p>
    </div>
  </div>


<div class="col-sm-6">
  <div class="card">
    <div class="card-header"> Desde {{$viajesTematicos->precio}} € </div>
    </div>
  <div class="card-body">
    <p class="card-text">Incluye: Vuelos de ida y vuelta, las noches en el hotel, traslados,seguro de viaje y médico, el guía multilenguaje experto en la temática.</p>
    <p class="card-text">No incluye: Las comidas fuera del hotel. Ni nada que no este en el apartado de incluye.</p>
  </div>
  <div class="card-footer">
    <a href="#" class="btn btn-success">Guardar  en cesta</a>
    <a href="/viajesTematicos" class="btn btn-primary">Volver a home</a>
  </div>
</div>

</div>
@endsection
