@extends('layouts.app')

@section('content')
<a href="/viajesTematicos/create" class="btn btn-success" style="margin-left: 15px">Crear Viaje</a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($viajesTematicos as $viajesTematicos)
    <div class="col mb-2">
        <div class="card h-100" style="width:450px">
            <div class="card-header">{{$viajesTematicos->titulo}}</div>
             <div class="card-body">
                    <a href="/viajesTematicos/{{$viajesTematicos->cod_viajesTematicos}}" class="btn btn-success">Información del Viaje</a>
                   <a href="/viajesTematicos/{{$viajesTematicos->cod_viajesTematicos}}/edit" class="btn btn-primary">Modificar Viaje</a>
                    <form method="post" action="/viajesTematicos/{{$viajesTematicos->cod_viajesTematicos}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
