@extends('layouts.app')

@section('content')
<a href="/viajesNovios/create" class="btn btn-success" style="margin-left: 15px">Crear Viaje</a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($viajeNo as $viajeNo)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/viajesNovios/{{$viajeNo->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$viajeNo->titulo}} desde {{$viajeNo->precio}} € </h5>
                    <a href="/viajesNovios/{{$viajeNo->cod_viajesNovios}}" class="btn btn-success">Información del Viaje</a>
                    <a href="/viajesNovios/{{$viajeNo->cod_viajesNovios}}/edit" class="btn btn-primary">Modificar Viaje</a>
                    <form method="post" action="/viajesNovios/{{$viajeNo->cod_viajesNovios}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
