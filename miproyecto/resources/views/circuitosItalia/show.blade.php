@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-sm-6">
      <div class="card pmd-card">
        <div class="card-header d-flex flex-row">
          <h3 class="card-title">{{$circuitosItalia->titulo}}</h3>
         </div>

        <div class="card-body">
          <p class="card-text">Limite de plazas máximo de :{{$circuitosItalia->numPersona}} personas.</p>
          <p class="card-text">{{$circuitosItalia->descripcion}}</p>
        </div>

      </div>
    </div>



    <div class="col-sm-6">
       <div class="card">
          <div class="card-header"> Intinerario </div>
       </div>
       <div class="card-body">
           <p class="card-text">{{$circuitosItalia->intinerario}}</p>
        </div>
    </div>



    <div class="col-sm-6">
       <div class="card" style="margin-top:5px; margin-right: 1px">
          <div class="card-header"> Precio del circuito {{$circuitosItalia->precio}} € </div>
       </div>
       <div class="card-body">
          <p class="card-text">Incluye: Las entradas a los monumentos y museos, las noches en los hoteles, traslados,seguro de viaje y médico, el guía multilenguaje .</p>
          <p class="card-text">No incluye: Las comidas. Ni nada que no este en el apartado de incluye.</p>
        </div>
      <div class="card-footer">
         <a href="#" class="btn btn-success">Guardar  en cesta</a>
         <a href="/circuitosItalia" class="btn btn-primary">Volver a home</a>
      </div>
    </div>



</div>
@endsection
