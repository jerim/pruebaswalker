@extends('layouts.app')

@section('content')
<a href="/circuitosItalia/create" class="btn btn-success" style="margin-left: 15px">Crear Circuito</a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($circuitosItalia as $circuitosItalia)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/circuitosItalia/{{$circuitosItalia->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$circuitosItalia->titulo}} desde {{$circuitosItalia->precio}} € </h5>
                    <a href="/circuitosItalia/{{$circuitosItalia->cod_circuito}}" class="btn btn-success">Información del Viaje</a>
                    <a href="/circuitosItalia/{{$circuitosItalia->cod_circuito}}/edit" class="btn btn-primary">Modificar Viaje</a>
                    <form method="post" action="/circuitosItalia/{{$circuitosItalia->cod_circuito}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
