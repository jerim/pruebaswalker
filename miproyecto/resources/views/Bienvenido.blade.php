@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row" style="margin-left: -10px">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bienvenido {{ucfirst(Auth()->user()->name)}} </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Estás registrado!
                </div>
                <div class="card-body">
                    <a class="btn btn-success"  href="/logout">Cerrar sessión</a>
                    <a class="btn btn-success"  href="/welcome">Página de inicio</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
