@extends('layouts.app')

@section('content')
<a href="/circuitosEmiratos/create" class="btn btn-success" style="margin-left: 15px">Crear Circuito</a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($circuitosEmirato as $circuitosEmirato)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/circuitosEmiratos/{{$circuitosEmirato->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$circuitosEmirato->titulo}} desde {{$circuitosEmirato->precio}} € </h5>
                    <a href="/circuitosEmiratos/{{$circuitosEmirato->cod_circuitoEm}}" class="btn btn-success">Información del Viaje</a>
                    <a href="/circuitosEmiratos/{{$circuitosEmirato->cod_circuitoEm}}/edit" class="btn btn-primary">Modificar Viaje</a>
                    <form method="post" action="/circuitosEmiratos/{{$circuitosEmirato->cod_circuitoEm}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
