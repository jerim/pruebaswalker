@extends('layouts.app')

@section('content')
<a href="/circuitosNoruega/create" class="btn btn-success" style="margin-left: 15px">Crear Circuito</a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($circuitoNoruega as $circuitoNoruega)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/circuitosNoruega/{{$circuitoNoruega->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$circuitoNoruega->titulo}} desde {{$circuitoNoruega->precio}} € </h5>
                    <a href="/circuitosNoruega/{{$circuitoNoruega->cod_circuitoNo}}" class="btn btn-success">Información del Viaje</a>
                    <a href="/circuitosNoruega/{{$circuitoNoruega->cod_circuitoNo}}/edit" class="btn btn-primary">Modificar Viaje</a>
                    <form method="post" action="/circuitosNoruega/{{$circuitoNoruega->cod_circuitoNo}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
