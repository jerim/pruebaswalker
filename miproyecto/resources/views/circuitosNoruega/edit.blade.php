@extends('layouts.app')
@section('content')

<div class="container" style="margin-left: 2px; margin-bottom: 2px;">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Modificar circuito </h1>
         <form method="POST" action="/circuitosNoruega/{{$circuitoNoruega->cod_circuitoNo}}" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="_method" value="PUT">


            <div class="form-group">
                <label>Titulo</label>
                <input class="form-control" type="text" name="titulo" value="{{$circuitoNoruega->titulo}}">
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" value="{{$circuitoNoruega->descripcion}}">
            </div>

            <div class="form-group">
                <label>Itinerario</label>
                <input class="form-control" type="text" name="intinerario" value="{{$circuitoNoruega->intinerario}}">
            </div>


            <div class="form-group">
                <label>Numero de personas</label>
                <input class="form-control" type="Integer" name="numPersona" value="{{$circuitoNoruega->numPersona}}">
            </div>


            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" value="{{$circuitoNoruega->precio}}">
            </div>

             <div class="form-group">
                <label>Imagen</label>
                <input accept="image/*" type="file" name="file" value="file">
            </div>


            <input type="submit" class="btn btn-success" name="Modificar" value="Modificar">
            <a href="/circuitosNoruega" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->


@endsection
