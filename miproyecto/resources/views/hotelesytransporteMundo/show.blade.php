@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-sm-6">
      <div class="card pmd-card">
        <div class="card-header d-flex flex-row">
          <h3 class="card-title">{{$hotelTransMundo->nombre}}</h3>
         </div>

        <div class="card-body">
          <p class="card-text">{{$hotelTransMundo->descripcion}}</p>
        </div>

      </div>
    </div>

    <div class="col-sm-6">
       <div class="card" style="margin-top:5px; margin-right: 1px">
          <div class="card-header"> Precio del hotel {{$hotelTransMundo->precio}} € </div>
       </div>
       <div class="card-body">
          <p class="card-text">Pension :{{$hotelTransMundo->pension}}</p>
          <p class="card-text">Numero de personas :{{$hotelTransMundo->numPersona}}</p>
          <p class="card-text">Numero de habitaciones :{{$hotelTransMundo->numHabitaciones}}</p>
        </div>
      <div class="card-footer">
         <a href="#" class="btn btn-success">Guardar  en cesta</a>
         <a href="/hotelesytransporteMundo" class="btn btn-primary">Volver a home</a>
      </div>
    </div>



</div>
@endsection
