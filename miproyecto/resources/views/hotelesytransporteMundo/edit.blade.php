@extends('layouts.app')
@section('content')

<div class="container" style="margin-left: 2px; margin-bottom: 2px;">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Modificar hoteles y transporte mundo </h1>
         <form method="POST" action="/hotelesytransporteMundo/{{$hotelTransMundo->cod_hotelytransporte}}" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="_method" value="PUT">


            <div class="form-group">
                <label>Nombre</label>
                <input class="form-control" type="text" name="nombre" value="{{$hotelTransMundo->nombre}}">
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" value="{{$hotelTransMundo->descripcion}}">
            </div>

            <div class="form-group">
                <label>Pension</label>
                <input class="form-control" type="text" name="pension" value="{{$hotelTransMundo->pension}}">
            </div>

            <div class="form-group">
                <label>Tipo de transporte</label>
                <input class="form-control" type="text" name="tipoTransporte" value="{{$hotelTransMundo->tipoTransporte}}">
            </div>

            <div class="form-group">
                <label>Numero de personas</label>
                <input class="form-control" type="Integer" name="numPersona" value="{{$hotelTransMundo->numPersona}}">
            </div>

            <div class="form-group">
                <label>Numero de habitaciones</label>
                <input class="form-control" type="Integer" name="numHabitaciones" value="{{$hotelTransMundo->numHabitaciones}}">
            </div>

            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" value="{{$hotelTransMundo->precio}}">
            </div>

            <div class="form-group">
                <label>Imagen</label>
                <input accept="image/*" type="file" name="file" value="file">
            </div>


            <input type="submit" class="btn btn-success" name="Modificar" value="Modificar">
            <a href="/hotelesytransporteMundo" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->


@endsection
