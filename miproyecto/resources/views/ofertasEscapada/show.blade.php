@extends('layouts.app')
@section('content')
<div class="card pmd-card">
  <div class="card-body d-flex flex-row">
    <div class="media-body">
        <h2 class="card-title">{{$ofertasEscapada->nombre}} desde {{$ofertasEscapada->precio}} €</h2>
        <p class="card-subtitle">{{$ofertasEscapada->descripcion}}</p>
    </div>
     <img src="/imagenes/ofertasEscapada/{{$ofertasEscapada->rutaImg}}" width="112" height="112" />
  </div>
  <div class="card-footer">
    <a href="#" class="btn btn-success">Guardar  en cesta</a>
    <a href="/ofertasEscapada" class="btn btn-primary">Volver a home</a>
  </div>
</div>

@endsection
