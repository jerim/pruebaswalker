@extends('layouts.app')

@section('content')
<a href="/ofertasEscapada/create" class="btn btn-success" style="margin-left: 15px">Crear oferta de escapada </a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($ofertasEscapada as $ofertasEscapada)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/ofertasEscapada/{{$ofertasEscapada->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$ofertasEscapada->nombre}} desde {{$ofertasEscapada->precio}} € </h5>
                    <a href="/ofertasEscapada/{{$ofertasEscapada->cod_oferta}}" class="btn btn-success">Información del hotel y transporte</a>
                    <a href="/ofertasEscapada/{{$ofertasEscapada->cod_oferta}}/edit" class="btn btn-primary">Modificar hotel y transporte</a>
                    <form method="post" action="/ofertasEscapada/{{$ofertasEscapada->cod_oferta}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
