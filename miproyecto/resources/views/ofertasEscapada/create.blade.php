@extends('layouts.app')
@section('content')

<div class="container" style="margin-left:-10px">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Crear oferta de escapada</h1>
         <form method="POST" action="/ofertasEscapada" enctype="multipart/form-data">
            {{csrf_field()}}

            <div class="form-group">
                <label>Nombre</label>
                <input class="form-control" type="text" name="nombre" value="nombre">
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" value="descripcion">
            </div>


            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" value="precio">
            </div>

            <div class="form-group">
                <label>Imagen</label>
                <input accept="image/*" type="file" name="file" value="file">
            </div>


            <input type="submit" class="btn btn-success" name="Crear" value="Crear">
            <a href="/ofertasEscapada" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->
@endsection
