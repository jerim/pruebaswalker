@extends('layouts.app')
@section('content')

<div class="container" style="margin-left: 2px; margin-bottom: 2px;">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Modificar hoteles y transporte mundo </h1>
         <form method="POST" action="/ofertasUltimaHora/{{$ofertasUlHora->cod_ofertaUl}}" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="_method" value="PUT">


            <div class="form-group">
                <label>Nombre</label>
                <input class="form-control" type="text" name="nombre" value="{{$ofertasUlHora->nombre}}">
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" value="{{$ofertasUlHora->descripcion}}">
            </div>


            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" value="{{$ofertasUlHora->precio}}">
            </div>

            <div class="form-group">
                <label>Imagen</label>
                <input accept="image/*" type="file" name="file" value="file">
            </div>


            <input type="submit" class="btn btn-success" name="Modificar" value="Modificar">
            <a href="/ofertasUltimaHora" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->


@endsection
