@extends('layouts.app')
@section('content')
<div class="card pmd-card">
  <div class="card-body d-flex flex-row">
    <div class="media-body">
        <h2 class="card-title">{{$ofertasUlHora->nombre}} desde {{$ofertasUlHora->precio}} €</h2>
        <p class="card-subtitle">{{$ofertasUlHora->descripcion}}</p>
    </div>
     <img src="/imagenes/ofertasUltimaHora/{{$ofertasUlHora->rutaImg}}" width="112" height="112" />
  </div>
  <div class="card-footer">
    <a href="#" class="btn btn-success">Guardar  en cesta</a>
    <a href="/ofertasUltimaHora" class="btn btn-primary">Volver a home</a>
  </div>
</div>

@endsection
