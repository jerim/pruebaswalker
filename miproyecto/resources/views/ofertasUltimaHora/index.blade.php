@extends('layouts.app')

@section('content')
<a href="/ofertasUltimaHora/create" class="btn btn-success" style="margin-left: 15px">Crear oferta de ultima hora</a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($ofertasUlHora as $ofertasUlHora)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/ofertasUltimaHora/{{$ofertasUlHora->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$ofertasUlHora->nombre}} desde {{$ofertasUlHora->precio}} € </h5>
                    <a href="/ofertasUltimaHora/{{$ofertasUlHora->cod_ofertaUl}}" class="btn btn-success">Información del hotel y transporte</a>
                    <a href="/ofertasUltimaHora/{{$ofertasUlHora->cod_ofertaUl}}/edit" class="btn btn-primary">Modificar hotel y transporte</a>
                    <form method="post" action="/ofertasUltimaHora/{{$ofertasUlHora->cod_ofertaUl}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
