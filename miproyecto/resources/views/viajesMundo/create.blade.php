@extends('layouts.app')
@section('content')
<div class="container" style="margin-left:-10px">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Crear Viajes </h1>
         <form method="POST" action="/viajesMundo" enctype="multipart/form-data">
            {{csrf_field()}}

            <div class="form-group">
                <label>Titulo</label>
                <input class="form-control" type="text" name="titulo" >
            </div>

             <div class="form-group">
                <label>Origen</label>
                <input class="form-control" type="text" name="origenYdestino" >
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" >
            </div>

            <div class="form-group">
                <label>Numero de personas</label>
                <input class="form-control" type="Integer" name="numPersona" >
            </div>


            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" >
            </div>

            <div class="form-group">
                <label>Imagen</label>
                <input accept="image/*" type="file" name="file" value="file">
            </div>


            <input type="submit" class="btn btn-success" name="Crear" value="Crear">
            <a href="/viajesMundo" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->
@endsection
