@extends('layouts.app')

@section('content')
<a href="/viajesMundo/create" class="btn btn-success" style="margin-left: 15px">Crear Viaje</a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($viaje as $viaje)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/viajes/{{$viaje->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$viaje->titulo}} desde {{$viaje->precio}} € </h5>
                    <a href="/viajesMundo/{{$viaje->cod_viajes}}" class="btn btn-success">Información Viaje</a>
                    <a href="/viajesMundo/{{$viaje->cod_viajes}}/edit" class="btn btn-primary">Modificar Viaje</a>
                    <form method="post" action="/viajesMundo/{{$viaje->cod_viajes}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
