@extends('layouts.app')
@section('content')

<div class="container" style="margin-left: 2px; margin-bottom: 2px;">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Modificar Viajes </h1>
         <form method="POST" action="/viajesMundo/{{$viaje->cod_viajes}}" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="_method" value="PUT">


            <div class="form-group">
                <label>Titulo</label>
                <input class="form-control" type="text" name="titulo" value="{{$viaje->titulo}}">
            </div>

             <div class="form-group">
                <label>Origen</label>
                <input class="form-control" type="text" name="origenYdestino" value="{{$viaje->origenYdestino}}">
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" value="{{$viaje->descripcion}}">
            </div>

            <div class="form-group">
                <label>Numero de personas</label>
                <input class="form-control" type="Integer" name="numPersona" value="{{$viaje->numPersona}}">
            </div>


            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" value="{{$viaje->precio}}">
            </div>

             <div class="form-group">
                <label>Imagen</label>
                <input accept="image/*" type="file" name="file" value="file">
            </div>


            <input type="submit" class="btn btn-success" name="Modificar" value="Modificar">
            <a href="/viajesMundo" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->


@endsection
