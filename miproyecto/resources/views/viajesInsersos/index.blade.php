@extends('layouts.app')

@section('content')
<a href="/viajesInserso/create" class="btn btn-success" style="margin-left: 15px">Crear Viaje</a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($viajeInsersos as $viajeInsersos)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/viajesInserso/{{$viajeInsersos->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$viajeInsersos->titulo}} desde {{$viajeInsersos->precio}} € </h5>
                    <a href="/viajesInserso/{{$viajeInsersos->cod_viajesInsersos}}" class="btn btn-success">Información del Viaje</a>
                    <a href="/viajesInserso/{{$viajeInsersos->cod_viajesInsersos}}/edit" class="btn btn-primary">Modificar Viaje</a>
                    <form method="post" action="/viajesInserso/{{$viajeInsersos->cod_viajesInsersos}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
