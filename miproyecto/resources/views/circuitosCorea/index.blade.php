@extends('layouts.app')

@section('content')
<a href="/circuitosCorea/create" class="btn btn-success" style="margin-left: 15px">Crear Circuito</a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($circuitosCorea as $circuitosCorea)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/circuitosCorea/{{$circuitosCorea->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$circuitosCorea->titulo}} desde {{$circuitosCorea->precio}} € </h5>
                    <a href="/circuitosCorea/{{$circuitosCorea->cod_circuitoCo}}" class="btn btn-success">Información del Viaje</a>
                    <a href="/circuitosCorea/{{$circuitosCorea->cod_circuitoCo}}/edit" class="btn btn-primary">Modificar Viaje</a>
                    <form method="post" action="/circuitosCorea/{{$circuitosCorea->cod_circuitoCo}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
