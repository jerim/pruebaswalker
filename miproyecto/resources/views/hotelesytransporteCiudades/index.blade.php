@extends('layouts.app')

@section('content')
<a href="/hotelesytransporteCiudades/create" class="btn btn-success" style="margin-left: 15px">Crear hotel y transporte</a>

<div class="card-deck" style="margin-top:5px; margin-right: 1px">
  @foreach($hotelTransCiudad as $hotelTransCiudad)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/hotelytransporteCiudades/{{$hotelTransCiudad->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$hotelTransCiudad->nombre}} desde {{$hotelTransCiudad->precio}} € </h5>
                    <a href="/hotelesytransporteCiudades/{{$hotelTransCiudad->cod_hotelytransporteCi}}" class="btn btn-success">Información del hotel y transporte</a>
                    <a href="/hotelesytransporteCiudades/{{$hotelTransCiudad->cod_hotelytransporteCi}}/edit" class="btn btn-primary">Modificar hotel y transporte</a>
                    <form method="post" action="/hotelesytransporteCiudades/{{$hotelTransCiudad->cod_hotelytransporteCi}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                </div>
        </div>
    </div>
  @endforeach
</div>

@endsection
