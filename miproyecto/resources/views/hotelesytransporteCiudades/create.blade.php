@extends('layouts.app')
@section('content')

<div class="container" style="margin-left:-10px">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Crear hotel y transporte </h1>
         <form method="POST" action="/hotelesytransporteCiudades" enctype="multipart/form-data">
            {{csrf_field()}}

            <div class="form-group">
                <label>Nombre</label>
                <input class="form-control" type="text" name="nombre" value="nombre">
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" value="descripcion">
            </div>

            <div class="form-group">
                <label>Pension</label>
                <input class="form-control" type="text" name="pension" value="pension">
            </div>

            <div class="form-group">
                <label>Tipo de transporte</label>
                <input class="form-control" type="text" name="tipoTransporte" value="tipoTransporte">
            </div>

            <div class="form-group">
                <label>Numero de personas</label>
                <input class="form-control" type="Integer" name="numPersona" value="numPersona">
            </div>

            <div class="form-group">
                <label>Numero de habitaciones</label>
                <input class="form-control" type="Integer" name="numHabitaciones" value="numHabitaciones">
            </div>

            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" value="precio">
            </div>

            <div class="form-group">
                <label>Imagen</label>
                <input accept="image/*" type="file" name="file" value="file">
            </div>


            <input type="submit" class="btn btn-success" name="Crear" value="Crear">
            <a href="/hotelesytransporteCiudades" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->
@endsection
