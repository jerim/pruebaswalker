@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-sm-6">
      <div class="card pmd-card">
        <div class="card-header d-flex flex-row">
          <h3 class="card-title">{{$hotelTransCiudad->nombre}}</h3>
         </div>

        <div class="card-body">
          <p class="card-text">{{$hotelTransCiudad->descripcion}}</p>
        </div>

      </div>
    </div>

    <div class="col-sm-6">
       <div class="card" style="margin-top:5px; margin-right: 1px">
          <div class="card-header"> Precio del hotel {{$hotelTransCiudad->precio}} € </div>
       </div>
       <div class="card-body">
          <p class="card-text">Pension :{{$hotelTransCiudad->pension}}</p>
          <p class="card-text">Numero de personas :{{$hotelTransCiudad->numPersona}}</p>
          <p class="card-text">Numero de habitaciones :{{$hotelTransCiudad->numHabitaciones}}</p>
        </div>
      <div class="card-footer">
         <a href="#" class="btn btn-success">Guardar  en cesta</a>
         <a href="/hotelesytransporteCiudades" class="btn btn-primary">Volver a home</a>
      </div>
    </div>



</div>
@endsection
