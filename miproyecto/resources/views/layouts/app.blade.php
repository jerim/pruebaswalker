<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--CSRF TOKEN -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title> Agencia de viajes Walker</title>



        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

 <!-- Styles -->
        {{--  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
<style>
   html, body {
       background-color: #fff;
       color: #636b6f;
       font-family: 'Nunito', sans-serif;
       font-weight: 20;
       height: 100vh;
       margin: 0;
    }

    .full-height {
       height: 100vh;
    }


/*Titulo de la página*/
    .navbar-brand {
       margin-left: 2px;

    }

/*Logo de la página*/
    .logo {
        width:120px;
        float:left;
    }

/*Menú de la página*/
    .navbar-nav {
        background-color: #86B22C;
    }

    #navbarDropdownMenuLink {
        color:white;
    }

    .dropdown-menu {
         background-color: #86B22C;
    }

/*pie de página*/
    .container {
         width: 140%;
         float: left;
         margin-left:-18px;
    }

    .nav {
         background-color:#86B22C;
         width:121.5%;
    }


    .nav-item a {
        color:white;
    }

    .nav-icon {
        margin-left:10px;
    }

/*Iconos */
    #icono {
        width:30px;
    }

</style>

</head>

<body>

<!--Titulo y menú de la página-->

<div class="content">
    <nav class="navbar navbar-expand-lg navbar-light ">
    <img class="logo" src="imagenes/Logo/logo.PNG"/> <a class="navbar-brand" href="#">Agencia de viajes Walker</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span>
     </button>

    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" id="navbarDropdownMenuLink" href="/welcome">
                    <svg id="icono" class="bi bi-house" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                       <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"/>
                       <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z" clip-rule="evenodd"/>
                    </svg>
                </a>
            </li>

            <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Viajes</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/viajesMundo">Descubre mundo</a>
                                <a class="dropdown-item" href="/viajesNovios">Novios</a>
                                <a class="dropdown-item" href="/viajesTematicos">Tematicos</a>
                                <a class="dropdown-item" href="/viajesInserso">Inserso</a>
                         </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Excursiones</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/excursionMundo">Por el mundo</a>
                                <a class="dropdown-item" href="/excursionRecorrido">Recorridos</a>
                                <a class="dropdown-item" href="/excursionCultural">Culturales</a>
                        </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Circuitos</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/circuitosItalia">Por Italia</a>
                                <a class="dropdown-item" href="/circuitosEmiratos">Por Emiratos Árabes</a>
                                <a class="dropdown-item" href="/circuitosCorea">Por Corea del sur</a>
                                <a class="dropdown-item" href="/circuitosNoruega">Por Noruega</a>
                                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hoteles y Transporte
                </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/hotelesytransporteMundo">En el mundo</a>
                                <a class="dropdown-item" href="/hotelesytransporteCiudades">Ciudades</a>
                        </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ofertas</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/ofertasEscapada">De escapadas</a>
                                <a class="dropdown-item" href="/ofertasUltimaHora">De última hora</a>
                        </div>
            </li>

            <li class="nav-item ">
                <a class="nav-link" id="navbarDropdownMenuLink" href="/cesta">
                    <svg id="icono" class="bi bi-bag" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M14 5H2v9a1 1 0 001 1h10a1 1 0 001-1V5zM1 4v10a2 2 0 002 2h10a2 2 0 002-2V4H1z" clip-rule="evenodd"/>
                    <path d="M8 1.5A2.5 2.5 0 005.5 4h-1a3.5 3.5 0 117 0h-1A2.5 2.5 0 008 1.5z"/>
                    </svg>
                    <span class="badge">{{Session::has('carta') ? Session::get('carta')->cantidadTotal : ''}} </span>
                </a>

            </li>

            <li class="nav-item">
                @if(Route::has('login'))
                    @auth
                      <a class="nav-link" id="navbarDropdownMenuLink" href="{{ url('/home') }}">Tu cuenta</a>
                    @else
                      <a class="nav-link" id="navbarDropdownMenuLink" href="{{ route('login') }}">Inicia sessión</a>
                    @endauth
                @endif
            </li>

            <li class="nav-item">
                @if (Route::has('register'))
                    <a class="nav-link" id="navbarDropdownMenuLink" href="{{ route('register') }}">Registrate</a>
                @endif
            </li>


        </ul>
    </div>
 </nav>


@yield('content')


 <!--Pie  de la página-->
<footer class="footer mt-auto py-3">
        <div class="container">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="/politicas">Politicas de privacidad</a>
                </li>

                <li class="nav-item ">
                    <a class="nav-link" href="/contactos">Contactanos</a>
                </li>

            </ul>
        </div>

</footer>


        </div>
    </body>
</html>





