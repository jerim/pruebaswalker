@extends('layouts.app')
@section('content')

<div class="container" style="margin-left: 2px; margin-bottom: 1px;">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Modificar excursiones mundo </h1>
         <form method="POST" action="/excursionMundo/{{$excursiones->cod_excursion}}" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="_method" value="PUT">


            <div class="form-group">
                <label>Titulo</label>
                <input class="form-control" type="text" name="titulo" value="{{$excursiones->titulo}}">
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" value="{{$excursiones->descripcion}}">
            </div>

            <div class="form-group">
                <label>Intinerario</label>
                <input class="form-control" type="text" name="Intinerario" value="{{$excursiones->intinerario}}">
            </div>


            <div class="form-group">
                <label>Numero de personas</label>
                <input class="form-control" type="Integer" name="numPersona" value="{{$excursiones->numPersona}}">
            </div>

            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" value="{{$excursiones->precio}}">
            </div>




            <input type="submit" class="btn btn-success" name="Modificar" value="Modificar">
            <a href="/excursionMundo" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->


@endsection

