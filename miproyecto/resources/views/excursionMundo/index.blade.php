@extends('layouts.app')

@section('content')
 <a href="/excursionMundo/create" class="btn btn-success" style="margin-left: 15px">Crear excursion</a>
<div class="container">
<hr class="mt-2 mb-5">
    <div class="row text-center text-lg-left">
      @foreach($excursiones as $excursiones)
        <div class="col-lg-3 col-md-4 col-6">
         <div class="card border-secondary mb-3" style="max-width: 18rem; margin-left: 15px;">
            <div class="card-header">{{$excursiones->titulo}}</div>
            <div class="card-body text-secondary">
              <p class="card-text">
                <a href="/excursionMundo/{{$excursiones->cod_excursion}}" class="btn btn-success">Información de la excursion</a>
                <a href="/excursionMundo/{{$excursiones->cod_excursion}}/edit" class="btn btn-primary">Modificar excursion</a>
                <form method="post" action="/excursionMundo/{{$excursiones->cod_excursion}}">
                  {{csrf_field()}}
                    <input type="hidden" name="_method" value="delete">
                    <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                </form>
              </p>
            </div>
         </div>
        </div>
      @endforeach
    </div>
  </hr>
</div>

@endsection
