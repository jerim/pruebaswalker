@extends('layouts.app')
@section('content')
@if(Session::has('carta'))
  <div class="row">
    <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
    <ul class="list-group">
        @foreach($viaje as $viaje)
          <li class="list-group-item d-flex justify-content-between aling-items-center">
            <strong>Viaje {{$viaje['objeto']['titulo']}}</strong>
            <strong>Precio: {{$viaje['precio']}} €</strong>
            <span class="badge badge-primary badge-pill">{{$viaje['cantidad']}}</span>


             <a href="/removerUno/{{$viaje['objeto']['cod_viajes']}} " class="badge badge-danger badge-pill"> - </a>
              <a href="#" class="badge badge-danger badge-pill">Elimina este entero</a>
            </li>
        @endforeach
        </ul>
    </div>
  </div>

  <hr>

  <div class="row">
    <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
      <strong>Total a pagar : {{$precioTotal}} € </strong>
    </div>
  </div>

  <hr>

  <div class="row">
    <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
      <a href="/removerTodo" class="btn btn-danger">Eliminar la cesta</a>
      <a href="#" class="btn btn-success">Comprar</a>
      <a href="#" class="btn btn-success">Presuesto</a>
    </div>
  </div>

@else

  <div class="row">
    <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
      <h2>No hay elementos en la cesta</h2>
    </div>
  </div>

@endif
@endsection

