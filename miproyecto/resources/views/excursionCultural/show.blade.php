@extends('layouts.app')
@section('content')
<div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-header">{{$excursionesCulturales->titulo}}</div>
    </div>
    <div class="card-body">
      <p class="card-text">Limite de plazas máximo de :{{$excursionesCulturales->numPersona}} personas.</p>
      <p class="card-text">{{$excursionesCulturales->descripcion}}</p>
      <p class="card-text">{{$excursionesCulturales->intinerario}}</p>
    </div>
  </div>


<div class="col-sm-6">
  <div class="card">
    <div class="card-header"> Precio de la excursion {{$excursionesCulturales->precio}} € </div>
    </div>
  <div class="card-body">
    <p class="card-text">Incluye: Las entradas a los monumentos y museos, traslados,seguro de viaje y médico, el guía multilenguaje .</p>
    <p class="card-text">No incluye: Las comidas. Ni nada que no este en el apartado de incluye.</p>
  </div>
  <div class="card-footer">
    <a href="#" class="btn btn-success">Guardar  en cesta</a>
    <a href="/excursionCultural" class="btn btn-primary">Volver a home</a>
  </div>
</div>

</div>
@endsection
