@extends('layouts.app')
@section('content')

<div class="container" style="margin-left:-10px">
  <div class="row justify-content-center">
      <div class="col-md-12">
         <h1> Crear excursion mundo </h1>
         <form method="POST" action="/excursionCultural" enctype="multipart/form-data">
            {{csrf_field()}}

            <div class="form-group">
                <label>Titulo</label>
                <input class="form-control" type="text" name="titulo" value="titulo">
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <input class="form-control" type="text" name="descripcion" value="descripcion">
            </div>


             <div class="form-group">
                <label>Intinerario</label>
                <input class="form-control" type="text" name="intinerario" value="intinerario">
            </div>


            <div class="form-group">
                <label>Numero de personas</label>
                <input class="form-control" type="Integer" name="numPersona" value="numPersona">
            </div>


            <div class="form-group">
                <label>Precio</label>
                <input class="form-control" type="Integer" name="precio" value="precio">
            </div>


            <input type="submit" class="btn btn-success" name="Crear" value="Crear">
            <a href="/excursionCultural" class="btn btn-success">Volver a home</a>
         </form>



      </div><!--col md -->
    </div><!--col justify -->
</div> <!--container -->
@endsection
