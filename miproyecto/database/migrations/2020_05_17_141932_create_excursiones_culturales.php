<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExcursionesCulturales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excursiones_culturales', function (Blueprint $table) {
            $table->bigIncrements('cod_excursionCultural')->unsigned();
            $table->string('titulo');
            $table->text('descripcion');
            $table->text('intinerario');
            $table->integer('numPersona');
            $table->integer('precio');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('excursiones_culturales');
    }
}
