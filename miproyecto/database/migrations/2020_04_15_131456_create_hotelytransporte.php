<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelytransporte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotelytransporte', function (Blueprint $table) {
            $table->bigIncrements('cod_hotelytransporte')->unsigned();
            $table->string('nombre');
            $table->text('descripcion');
            $table->string('pension');
            $table->string('tipoTransporte');
            $table->integer('numPersona');
            $table->integer('numHabitaciones');
            $table->integer('precio');
            $table->string('rutaImg');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotelytransporte');
    }
}
