<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCircuitosEmiratos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circuitos_emiratos', function (Blueprint $table) {
            $table->bigIncrements('cod_circuitoEm')->unsigned();
            $table->string('titulo');
            $table->text('descripcion');
            $table->text('intinerario');
            $table->integer('numPersona');
            $table->integer('precio');
            $table->string('rutaImg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('circuitos_emiratos');
    }
}
