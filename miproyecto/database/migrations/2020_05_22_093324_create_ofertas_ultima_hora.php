<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfertasUltimaHora extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertas_ultima_hora', function (Blueprint $table) {
            $table->bigIncrements('cod_ofertaUl')->unsigned();
            $table->string('nombre');
            $table->text('descripcion');
            $table->integer('precio');
            $table->string('rutaImg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofertas_ultima_hora');
    }
}
