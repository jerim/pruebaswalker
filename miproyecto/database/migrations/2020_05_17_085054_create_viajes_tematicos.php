<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViajesTematicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajes_tematicos', function (Blueprint $table) {
            $table->bigIncrements('cod_viajesTematicos')->unsigned();
            $table->string('titulo');
            $table->text('descripcion');
            $table->integer('numPersona');
            $table->string('origenYdestino');
            $table->integer('precio');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viajes_tematicos');
    }
}
