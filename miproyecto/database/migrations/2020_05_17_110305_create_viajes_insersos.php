<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViajesInsersos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajes_insersos', function (Blueprint $table) {
            $table->bigIncrements('cod_viajesInsersos')->unsigned();
            $table->string('titulo');
            $table->text('descripcion');
            $table->integer('numPersona');
            $table->string('origenYdestino');
            $table->integer('precio');
            $table->string('rutaImg');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viajes_insersos');
    }
}
