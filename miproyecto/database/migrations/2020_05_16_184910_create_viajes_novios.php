<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViajesNovios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajes_novios', function (Blueprint $table) {
            $table->bigIncrements('cod_viajesNovios')->unsigned();
            $table->string('titulo');
            $table->string('informacion');
            $table->integer('numPersona');
            $table->string('origenYdestino');
            $table->integer('precio');
            $table->string('rutaImg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viajes_novios');
    }
}
