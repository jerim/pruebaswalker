<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExcursionRecorrido extends Model
{
    protected $fillable=['titulo','descripcion','intinerario','numPersona','origenYdestino','precio'];

    protected $primaryKey='cod_excursionRecorrido';

    public $table="excursiones_recorridos";

     public function users() {
        return $this->belongTo(User::class);
    }


    public function viajes() {
        return $this->belongTo(Viaje::class);

    }

    public function circuitos() {
        return $this->belongTo(Circuito::class);
    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }
}
