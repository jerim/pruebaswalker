<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExcursionCultural extends Model
{
   protected $fillable=['titulo','descripcion','intinerario','numPersona','origenYdestino','precio'];

    protected $primaryKey='cod_excursionCultural';

    public $table="excursiones_culturales";

    public function users() {
        return $this->belongTo(User::class);
    }


    public function viajes() {
        return $this->belongTo(Viaje::class);

    }

    public function circuitos() {
        return $this->belongTo(Circuito::class);
    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }
}
