<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfertaUltimaHora extends Model
{
    protected $fillable=['nombre','descripcion','precio','rutaImg'];

    protected $primaryKey='cod_ofertaUl';

    public $table="ofertas_ultima_hora";

    public function viajes() {
        return $this->belongTo(Viajes::class);
    }

    public function excursiones() {
        return $this->belongTo(Excursion::class);
    }

    public function hotelesytransportes() {
        return $this->belongTo(Hotelesytransportes::class);

    }

    public function circuitos() {
        return $this->belongTo(Circuitos::class);
    }
}
