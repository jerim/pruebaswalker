<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CircuitoCorea extends Model
{
    protected $fillable=['titulo','descripcion','intinerario','numPersona','precio','rutaImg'];

    protected $primaryKey='cod_circuitoCo';

    public $table="circuitos_corea";

    public function viajes() {
        return $this->hasMany(Viajes::class);
    }

    public function excursiones() {
        return $this->hasMany(Excursion::class);
    }

    public function hotelesytransportes() {
        return $this->hasMany(Hotelytransporte::class);

    }

    public function users() {
        return $this->belongTo(User::class);
    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }

}
