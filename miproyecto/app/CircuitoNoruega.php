<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CircuitoNoruega extends Model
{
   protected $fillable=['titulo','descripcion','intinerario','numPersona','precio','rutaImg'];

    protected $primaryKey='cod_circuitoNo';

    public $table="circuitos_noruega";

    public function viajes() {
        return $this->hasMany(Viajes::class);
    }

    public function excursiones() {
        return $this->hasMany(Excursion::class);
    }

    public function hotelesytransportes() {
        return $this->hasMany(Hotelytransporte::class);

    }

    public function users() {
        return $this->belongTo(User::class);
    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }
}
