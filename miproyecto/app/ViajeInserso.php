<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViajeInserso extends Model
{
    protected $fillable=['titulo','descripcion','numPersona','origenYdestino','precio','rutaImg'];

    protected $primaryKey='cod_viajesInsersos';

    public $table="viajes_insersos";


    public function users() {
        return $this->belongsTo(User::class);
    }

    public function excursiones() {
        return $this->hasMany(Excursion::class);
    }

    public function hotelesytransportes() {
        return $this->belongsTo(Circuito::class);

    }

    public function circuitos() {
        return $this->belongTo(Hotelytransporte::class);
    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }
}
