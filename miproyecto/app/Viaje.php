<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viaje extends Model
{
    protected $fillable=['titulo','descripcion','numPersona','origenYdestino','precio','rutaImg'];

    protected $primaryKey='cod_viajes';

    public $table="viajes";


    public function users() {
        return $this->belongsTo(User::class);
    }

    public function excursiones() {
        return $this->hasMany(Excursion::class);
    }

    public function hotelesytransportes() {
        return $this->belongsTo(Circuito::class);

    }

    public function circuitos() {
        return $this->belongTo(Hotelytransporte::class);
    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }
}
