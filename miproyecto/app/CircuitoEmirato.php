<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CircuitoEmirato extends Model
{

    protected $fillable=['titulo','descripcion','intinerario','numPersona','precio','rutaImg'];

    protected $primaryKey='cod_circuitoEm';

    public $table="circuitos_emiratos";

    public function viajes() {
        return $this->hasMany(Viajes::class);
    }

    public function excursiones() {
        return $this->hasMany(Excursion::class);
    }

    public function hotelesytransportes() {
        return $this->hasMany(Hotelytransporte::class);

    }

    public function users() {
        return $this->belongTo(User::class);
    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }

}
