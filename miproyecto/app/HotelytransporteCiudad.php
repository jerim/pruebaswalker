<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelytransporteCiudad extends Model
{
    protected $fillable=['nombre','descripcion','pension','tipoTransporte','numPersona','numHabitaciones','precio','rutaImg'];

    protected $primaryKey='cod_hotelytransporteCi';

    public $table="hotelytransporte_ciudades";


    public function viajes() {
        return $this->belongTo(Viajes::class);
    }

    public function users() {
        return $this->belongTo(User::class);

    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }
}
