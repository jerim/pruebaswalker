<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExcursionCultural;
use DB;
use Auth;
use File;

class ExcursionCulturalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $excursionesCulturales= ExcursionCultural::all();
       return view('excursionCultural.index',['excursionesCulturales'=>$excursionesCulturales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $excursionesCulturales= ExcursionCultural::all();
       return view('excursionCultural.create',['excursionesCulturales'=>$excursionesCulturales]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $excursionesCulturales=$request->all();
        ExcursionCultural::create($excursionesCulturales);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_excursionCultural
     * @return \Illuminate\Http\Response
     */
    public function show($cod_excursionCultural)
    {
        $excursionesCulturales= ExcursionCultural::findOrFail($cod_excursionCultural);
        return view('excursionCultural.show',['excursionesCulturales'=>$excursionesCulturales]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_excursionCultural
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_excursionCultural)
    {
        $excursionesCulturales= ExcursionCultural::findOrFail($cod_excursionCultural);
        return view('excursionCultural.edit',['excursionesCulturales'=>$excursionesCulturales]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_excursionCultural
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_excursionCultural)
    {
        $excursionesCulturales= ExcursionCultural::findOrFail($cod_excursionCultural);
        $excursionesCulturales->fill($request->all());
        $excursionesCulturales->save();
        return redirect('/excursionCultural');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_excursionCultural
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_excursionCultural)
    {
        $excursionesCulturales= ExcursionCultural::findOrFail($cod_excursionCultural);
        $excursionesCulturales->delete();
        return back();
    }
}
