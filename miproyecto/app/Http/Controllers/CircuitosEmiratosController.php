<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CircuitoEmirato;
use DB;
use Auth;
use File;

class CircuitosEmiratosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $circuitosEmirato= CircuitoEmirato::all();
       return view('circuitosEmiratos.index',['circuitosEmirato'=>$circuitosEmirato]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $circuitosEmirato= CircuitoEmirato::all();
       return view('circuitosEmiratos.create',['circuitosEmirato'=>$circuitosEmirato]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/circuitosEmiratos',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        CircuitoEmirato::create($entrada);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_circuitoEm
     * @return \Illuminate\Http\Response
     */
    public function show($cod_circuitoEm)
    {
       $circuitosEmirato= CircuitoEmirato::findOrFail($cod_circuitoEm);
       return view('circuitosEmiratos.show',['circuitosEmirato'=>$circuitosEmirato]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_circuitoEm
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_circuitoEm)
    {
       $circuitosEmirato= CircuitoEmirato::findOrFail($cod_circuitoEm);
       return view('circuitosEmiratos.edit',['circuitosEmirato'=>$circuitosEmirato]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_circuitoEm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_circuitoEm)
    {
        $circuitosEmirato= CircuitoEmirato::findOrFail($cod_circuitoEm);
        $circuitosEmirato->fill($request->all());

        //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/circuitosEmiratos', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $circuitosEmirato->rutaImg = $nombre;
        }


        $circuitosEmirato->save();
        return redirect("/circuitosEmiratos");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_circuitoEm
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_circuitoEm)
    {
        $circuitosEmirato= CircuitoEmirato::findOrFail($cod_circuitoEm);
        $circuitosEmirato->delete();
        return back();
    }
}
