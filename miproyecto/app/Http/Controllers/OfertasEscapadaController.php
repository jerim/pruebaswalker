<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Oferta;
use DB;
use Auth;
use File;

class OfertasEscapadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $ofertasEscapada= Oferta::all();
       return view('ofertasEscapada.index',['ofertasEscapada'=>$ofertasEscapada]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $ofertasEscapada= Oferta::all();
       return view('ofertasEscapada.create',['ofertasEscapada'=>$ofertasEscapada]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/ofertasEscapada',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        Oferta::create($entrada);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_oferta
     * @return \Illuminate\Http\Response
     */
    public function show($cod_oferta)
    {
       $ofertasEscapada= Oferta::findOrFail($cod_oferta);
       return view('ofertasEscapada.show',['ofertasEscapada'=>$ofertasEscapada]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_oferta
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_oferta)
    {
       $ofertasEscapada= Oferta::findOrFail($cod_oferta);
       return view('ofertasEscapada.edit',['ofertasEscapada'=>$ofertasEscapada]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_oferta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_oferta)
    {
        $ofertasEscapada= Oferta::findOrFail($cod_oferta);
        $ofertasEscapada->fill($request->all());

        //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/ofertasEscapada', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $ofertasEscapada->rutaImg = $nombre;
        }

        $ofertasEscapada->save();
        return redirect("/ofertasEscapada");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_oferta
     * @return \Illuminate\Http\Response
     */

    public function destroy($cod_oferta)
    {
        $ofertasEscapada= Oferta::findOrFail($cod_oferta);
        $ofertasEscapada->delete();
        return back();
    }
}
