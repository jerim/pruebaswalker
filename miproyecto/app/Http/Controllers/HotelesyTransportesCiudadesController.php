<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HotelytransporteCiudad;
use DB;
use Auth;
use File;

class HotelesyTransportesCiudadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $hotelTransCiudad= HotelytransporteCiudad::all();
       return view('hotelesytransporteCiudades.index',['hotelTransCiudad'=>$hotelTransCiudad]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $hotelTransCiudad= HotelytransporteCiudad::all();
       return view('hotelesytransporteCiudades.create',['hotelTransCiudad'=>$hotelTransCiudad]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/hotelytransporteCiudades',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        HotelytransporteCiudad::create($entrada);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_hotelytransporteCi
     * @return \Illuminate\Http\Response
     */
    public function show($cod_hotelytransporteCi)
    {
       $hotelTransCiudad= HotelytransporteCiudad::findOrFail($cod_hotelytransporteCi);
       return view('hotelesytransporteCiudades.show',['hotelTransCiudad'=>$hotelTransCiudad]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_hotelytransporteCi
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_hotelytransporteCi)
    {
       $hotelTransCiudad= HotelytransporteCiudad::findOrFail($cod_hotelytransporteCi);
       return view('hotelesytransporteCiudades.edit',['hotelTransCiudad'=>$hotelTransCiudad]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_hotelytransporteCi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_hotelytransporteCi)
    {
        $hotelTransCiudad= HotelytransporteCiudad::findOrFail($cod_hotelytransporteCi);
        $hotelTransCiudad->fill($request->all());

       //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/hotelytransporteCiudades', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $hotelTransCiudad->rutaImg = $nombre;
        }

        $hotelTransCiudad->save();
        return redirect("/hotelesytransporteCiudades");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_hotelytransporteCi
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_hotelytransporteCi)
    {
        $hotelTransCiudad= HotelytransporteCiudad::findOrFail($cod_hotelytransporteCi);
        $hotelTransCiudad->delete();
        return back();
    }
}
