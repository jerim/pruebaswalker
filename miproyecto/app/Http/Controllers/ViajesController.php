<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Viaje;
use DB;
use Auth;
use File;
use Session;
use App\cestaViajesMundo;

class ViajesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
/*
    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }*/

    public function index()
    {
       $viaje= Viaje::all();
       return view('viajesMundo.index',['viaje'=>$viaje]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $viaje= Viaje::all();
        return view('viajesMundo.create',['viaje'=>$viaje]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/viajes',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        Viaje::create($entrada);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cod_viajes)
    {
      $viaje= Viaje::findOrFail($cod_viajes);
      return view('viajesMundo.show',['viaje'=>$viaje]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_viajes)
    {
         $viaje= Viaje::findOrFail($cod_viajes);
         return view('viajesMundo.edit',['viaje'=>$viaje]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_viajes)
    {

       $viaje= Viaje::findOrFail($cod_viajes);
       $viaje->fill($request->all());

         //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/viajes', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $viaje->rutaImg = $nombre;
        }

        $viaje->save();
        return redirect("/viajesMundo");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_viajes)
    {
        $viaje= Viaje::findOrFail($cod_viajes);
        $viaje->delete();
        return back();
    }

    /*Métodos cesta */


    public function añadirCesta(Request $request, $cod_viajes) {
        $viaje= Viaje::findOrFail($cod_viajes);
        $cardVieja = Session::has('carta') ? Session::get('carta') : null;
        $carta = new cestaViajesMundo($cardVieja);
        $carta->añadir($viaje,$viaje->$cod_viajes);

        $request->session()->put('carta',$carta);
         return redirect("/viajesMundo");

    }

    public function comprobarCesta(){
          if(!Session::has('carta')) {
            return view('cesta.indexViajes',['viaje'=>null]);
        }
        $cardVieja = Session::get('carta');
        $carta = new cestaViajesMundo($cardVieja);
        return view('cesta.indexViajes',['viaje'=> $carta->objetos, 'precioTotal'=> $carta->precioTotal]);
    }


    public function removerUnElemento($cod_viajes) {
         $cardVieja = Session::has('carta') ? Session::get('carta') : null;
         $carta = new cestaViajesMundo($cardVieja);
         $carta->remover($cod_viajes);
         Session::put('carta',$carta);
         return redirect("/cesta");
    }


    public function removerTodo(Request $request) {

       $request->session()->forget('carta');
       return redirect("/cesta");

    }


}

