<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CircuitoCorea;
use DB;
use Auth;
use File;

class CircuitosCoreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $circuitosCorea= CircuitoCorea::all();
       return view('circuitosCorea.index',['circuitosCorea'=>$circuitosCorea]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $circuitosCorea= CircuitoCorea::all();
       return view('circuitosCorea.create',['circuitosCorea'=>$circuitosCorea]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/circuitosCorea',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        CircuitoCorea::create($entrada);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_circuitoCo
     * @return \Illuminate\Http\Response
     */
    public function show($cod_circuitoCo)
    {
       $circuitosCorea= CircuitoCorea::findOrFail($cod_circuitoCo);
       return view('circuitosCorea.show',['circuitosCorea'=>$circuitosCorea]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_circuitoCo
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_circuitoCo)
    {
       $circuitosCorea= CircuitoCorea::findOrFail($cod_circuitoCo);
       return view('circuitosCorea.edit',['circuitosCorea'=>$circuitosCorea]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_circuitoCo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_circuitoCo)
    {
        $circuitosCorea= CircuitoCorea::findOrFail($cod_circuitoCo);
        $circuitosCorea->fill($request->all());

         //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/circuitosCorea', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $circuitosCorea->rutaImg = $nombre;
        }


        $circuitosCorea->save();
        return redirect("/circuitosCorea");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_circuitoCo
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_circuitoCo)
    {
        $circuitosCorea= CircuitoCorea::findOrFail($cod_circuitoCo);
        $circuitosCorea->delete();
        return back();
    }
}
