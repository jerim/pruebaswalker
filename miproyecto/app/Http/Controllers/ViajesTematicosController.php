<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ViajeTematico;
use DB;
use Auth;
use File;

class ViajesTematicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
      $viajesTematicos= ViajeTematico::all();
       return view('viajesTematicos.index',['viajesTematicos'=>$viajesTematicos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $viajesTematicos= ViajeTematico::all();
        return view('viajesTematicos.create',['viajesTematicos'=>$viajesTematicos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $viajesTematicos=$request->all();
         ViajeTematico::create($viajesTematicos);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cod_viajesTematicos)
    {

      $viajesTematicos= ViajeTematico::findOrFail($cod_viajesTematicos);
      return view('viajesTematicos.show',['viajesTematicos'=>$viajesTematicos]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_viajesTematicos
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_viajesTematicos)
    {
      $viajesTematicos= ViajeTematico::findOrFail($cod_viajesTematicos);
      return view('viajesTematicos.edit',['viajesTematicos'=>$viajesTematicos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_viajesTematicos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_viajesTematicos)
    {
      $viajesTematicos= ViajeTematico::findOrFail($cod_viajesTematicos);
      $viajesTematicos->fill($request->all());
      $viajesTematicos->save();
      return redirect('/viajesTematicos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_viajesTematicos
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_viajesTematicos)
    {
        $viajesTematicos= ViajeTematico::findOrFail($cod_viajesTematicos);
        $viajesTematicos->delete();
        return back();
    }
}
