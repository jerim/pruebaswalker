<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotelytransporte;
use DB;
use Auth;
use File;

class HotelesyTransportesMundoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $hotelTransMundo= Hotelytransporte::all();
       return view('hotelesytransporteMundo.index',['hotelTransMundo'=>$hotelTransMundo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $hotelTransMundo= Hotelytransporte::all();
       return view('hotelesytransporteMundo.create',['hotelTransMundo'=>$hotelTransMundo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/hotelytransporteMundo',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        Hotelytransporte::create($entrada);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_hotelytransporte
     * @return \Illuminate\Http\Response
     */
    public function show($cod_hotelytransporte)
    {
       $hotelTransMundo= Hotelytransporte::findOrFail($cod_hotelytransporte);
       return view('hotelesytransporteMundo.show',['hotelTransMundo'=>$hotelTransMundo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_hotelytransporte
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_hotelytransporte)
    {
       $hotelTransMundo= Hotelytransporte::findOrFail($cod_hotelytransporte);
       return view('hotelesytransporteMundo.edit',['hotelTransMundo'=>$hotelTransMundo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_hotelytransporte
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_hotelytransporte)
    {
        $hotelTransMundo= Hotelytransporte::findOrFail($cod_hotelytransporte);
        $hotelTransMundo->fill($request->all());

         //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/hotelytransporteMundo', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $hotelTransMundo->rutaImg = $nombre;
        }

        $hotelTransMundo->save();
        return redirect("/hotelesytransporteMundo");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_hotelytransporte
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_hotelytransporte)
    {
        $hotelTransMundo= Hotelytransporte::findOrFail($cod_hotelytransporte);
        $hotelTransMundo->delete();
        return back();
    }
}
