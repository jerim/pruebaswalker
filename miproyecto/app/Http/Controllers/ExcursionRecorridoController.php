<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExcursionRecorrido;
use DB;
use Auth;
use File;

class ExcursionRecorridoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $excursionesRecorridos= ExcursionRecorrido::all();
       return view('excursionRecorrido.index',['excursionesRecorridos'=>$excursionesRecorridos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $excursionesRecorridos= ExcursionRecorrido::all();
       return view('excursionRecorrido.create',['excursionesRecorridos'=>$excursionesRecorridos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $excursionesRecorridos=$request->all();
        ExcursionRecorrido::create($excursionesRecorridos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_excursionRecorrido
     * @return \Illuminate\Http\Response
     */
    public function show($cod_excursionRecorrido)
    {
        $excursionesRecorridos= ExcursionRecorrido::findOrFail($cod_excursionRecorrido);
        return view('excursionRecorrido.show',['excursionesRecorridos'=>$excursionesRecorridos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_excursionRecorrido
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_excursionRecorrido)
    {
        $excursionesRecorridos= ExcursionRecorrido::findOrFail($cod_excursionRecorrido);
        return view('excursionRecorrido.edit',['excursionesRecorridos'=>$excursionesRecorridos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_excursionRecorrido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_excursionRecorrido)
    {
        $excursionesRecorridos= ExcursionRecorrido::findOrFail($cod_excursionRecorrido);
        $excursionesRecorridos->fill($request->all());
        $excursionesRecorridos->save();
        return redirect('/excursionRecorrido');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_excursionRecorrido
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_excursionRecorrido)
    {
        $excursionesRecorridos= ExcursionRecorrido::findOrFail($cod_excursionRecorrido);
        $excursionesRecorridos->delete();
        return back();
    }
}
