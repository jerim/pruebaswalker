<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OfertaUltimaHora;
use DB;
use Auth;
use File;

class OfertasUltimaHoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $ofertasUlHora= OfertaUltimaHora::all();
       return view('ofertasUltimaHora.index',['ofertasUlHora'=>$ofertasUlHora]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $ofertasUlHora= OfertaUltimaHora::all();
       return view('ofertasUltimaHora.create',['ofertasUlHora'=>$ofertasUlHora]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/ofertasUltimaHora',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        OfertaUltimaHora::create($entrada);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_ofertaUl
     * @return \Illuminate\Http\Response
     */
    public function show($cod_ofertaUl)
    {
       $ofertasUlHora= OfertaUltimaHora::findOrFail($cod_ofertaUl);
       return view('ofertasUltimaHora.show',['ofertasUlHora'=>$ofertasUlHora]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_ofertaUl
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_ofertaUl)
    {
       $ofertasUlHora= OfertaUltimaHora::findOrFail($cod_ofertaUl);
       return view('ofertasUltimaHora.edit',['ofertasUlHora'=>$ofertasUlHora]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_ofertaUl
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_ofertaUl)
    {
        $ofertasUlHora= OfertaUltimaHora::findOrFail($cod_ofertaUl);
        $ofertasUlHora->fill($request->all());

        //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/ofertasUltimaHora', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $ofertasUlHora->rutaImg = $nombre;
        }

        $ofertasUlHora->save();
        return redirect("/ofertasUltimaHora");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_ofertaUl
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_ofertaUl)
    {
        $ofertasUlHora= OfertaUltimaHora::findOrFail($cod_ofertaUl);
        $ofertasUlHora->delete();
        return back();
    }
}
