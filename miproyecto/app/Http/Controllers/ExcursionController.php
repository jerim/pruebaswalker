<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Excursion;
use DB;
use Auth;
use File;

class ExcursionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $excursiones= Excursion::all();
       return view('excursionMundo.index',['excursiones'=>$excursiones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $excursiones= Excursion::all();
        return view('excursionMundo.create',['excursiones'=>$excursiones]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $excursiones=$request->all();
         Excursion::create($excursiones);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cod_excursion)
    {
      $excursiones= Excursion::findOrFail($cod_excursion);
      return view('excursionMundo.show',['excursiones'=>$excursiones]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_excursion
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_excursion)
    {
       $excursiones= Excursion::findOrFail($cod_excursion);
       return view('excursionMundo.edit',['excursiones'=>$excursiones]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_excursion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_excursion)
    {
       $excursiones= Excursion::findOrFail($cod_excursion);
       $excursiones->fill($request->all());
       $excursiones->save();
       return redirect('/excursionMundo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_excursion
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_excursion)
    {
       $excursiones= Excursion::findOrFail($cod_excursion);
       $excursiones->delete();
       return back();
    }
}
