<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CircuitoNoruega;
use DB;
use Auth;
use File;

class CircuitosNoruegaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $circuitoNoruega= CircuitoNoruega::all();
       return view('circuitosNoruega.index',['circuitoNoruega'=>$circuitoNoruega]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $circuitoNoruega= CircuitoNoruega::all();
       return view('circuitosNoruega.create',['circuitoNoruega'=>$circuitoNoruega]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/circuitosNoruega',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        CircuitoNoruega::create($entrada);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_circuitoNo
     * @return \Illuminate\Http\Response
     */
    public function show($cod_circuitoNo)
    {
       $circuitoNoruega= CircuitoNoruega::findOrFail($cod_circuitoNo);
       return view('circuitosNoruega.show',['circuitoNoruega'=>$circuitoNoruega]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_circuitoNo
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_circuitoNo)
    {
       $circuitoNoruega= CircuitoNoruega::findOrFail($cod_circuitoNo);
       return view('circuitosNoruega.edit',['circuitoNoruega'=>$circuitoNoruega]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_circuitoNo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_circuitoNo)
    {
        $circuitoNoruega= CircuitoNoruega::findOrFail($cod_circuitoNo);
        $circuitoNoruega->fill($request->all());

         //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/circuitosNoruega', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $circuitoNoruega->rutaImg = $nombre;
        }

        $circuitoNoruega->save();
        return redirect("/circuitosNoruega");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_circuitoNo
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_circuitoNo)
    {
        $circuitoNoruega= CircuitoNoruega::findOrFail($cod_circuitoNo);
        $circuitoNoruega->delete();
        return back();
    }
}
