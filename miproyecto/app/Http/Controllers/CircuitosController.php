<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Circuito;
use DB;
use Auth;
use File;

class CircuitosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
       $circuitosItalia= Circuito::all();
       return view('circuitosItalia.index',['circuitosItalia'=>$circuitosItalia]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $circuitosItalia= Circuito::all();
       return view('circuitosItalia.create',['circuitosItalia'=>$circuitosItalia]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/circuitosItalia',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        Circuito::create($entrada);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_circuito
     * @return \Illuminate\Http\Response
     */
    public function show($cod_circuito)
    {
       $circuitosItalia= Circuito::findOrFail($cod_circuito);
       return view('circuitosItalia.show',['circuitosItalia'=>$circuitosItalia]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_circuito
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_circuito)
    {
       $circuitosItalia= Circuito::findOrFail($cod_circuito);
       return view('circuitosItalia.edit',['circuitosItalia'=>$circuitosItalia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_circuito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_circuito)
    {
        $circuitosItalia= Circuito::findOrFail($cod_circuito);
        $circuitosItalia->fill($request->all());

          //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/circuitosItalia', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $circuitosItalia->rutaImg = $nombre;
        }


        $circuitosItalia->save();
        return redirect("/circuitosItalia");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_circuito
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_circuito)
    {
        $circuitosItalia= Circuito::findOrFail($cod_circuito);
        $circuitosItalia->delete();
        return back();
    }
}
