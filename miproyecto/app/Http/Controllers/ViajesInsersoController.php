<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ViajeInserso;
use DB;
use Auth;
use File;

class ViajesInsersoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
     $this->middleware('auth')->except('index','show');
    }


    public function index()
    {

      $viajeInsersos= ViajeInserso::all();
       return view('viajesInsersos.index',['viajeInsersos'=>$viajeInsersos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $viajeInsersos= ViajeInserso::all();
        return view('viajesInsersos.create',['viajeInsersos'=>$viajeInsersos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/viajesInserso',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        ViajeInserso::create($entrada);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cod_viajesInsersos
     * @return \Illuminate\Http\Response
     */
    public function show($cod_viajesInsersos)
    {
      $viajeInsersos= ViajeInserso::findOrFail($cod_viajesInsersos);
      return view('viajesInsersos.show',['viajeInsersos'=>$viajeInsersos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_viajesInsersos
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_viajesInsersos)
    {
      $viajeInsersos= ViajeInserso::findOrFail($cod_viajesInsersos);
      return view('viajesInsersos.edit',['viajeInsersos'=>$viajeInsersos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_viajesInsersos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_viajesInsersos)
    {
       $viajeInsersos= ViajeInserso::findOrFail($cod_viajesInsersos);
       $viajeInsersos->fill($request->all());

        //Comprueba imagen
        if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/viajesInserso', $nombre);

            //Si existe la imagen
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina.
            };

            $viajeInsersos->rutaImg = $nombre;
        }


        $viajeInsersos->save();
        return redirect("/viajesInsersos");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_viajesInsersos
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_viajesInsersos)
    {
       $viajeInsersos= ViajeInserso::findOrFail($cod_viajesInsersos);
       $viajeInsersos->delete();
       return back();
    }
}
