<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ViajeNovio;
use DB;
use Auth;
use File;

class ViajesNoviosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct() {
     $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
        $viajeNo= ViajeNovio::all();
       return view('viajesNovios.index',['viajeNo'=>$viajeNo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $viajeNo= ViajeNovio::all();
      return view('viajesNovios.create',['viajeNo'=>$viajeNo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/viajesNovios',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        ViajeNovio::create($entrada);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cod_viajesNovios)
    {
      $viajeNo= ViajeNovio::findOrFail($cod_viajesNovios);
      return view('viajesNovios.show',['viajeNo'=>$viajeNo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cod_viajesNovios
     * @return \Illuminate\Http\Response
     */
    public function edit($cod_viajesNovios)
    {
        $viajeNo= ViajeNovio::findOrFail($cod_viajesNovios);
        return view('viajesNovios.edit',['viajeNo'=>$viajeNo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cod_viajesNovios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cod_viajesNovios)
    {
        $viajeNo= ViajeNovio::findOrFail($cod_viajesNovios);
        $viajeNo->fill($request->all());

         //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/viajesNovios', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $viajeNo->rutaImg = $nombre;
        }


        $viajeNo->save();
        return redirect("/viajesNovios");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cod_viajesNovios
     * @return \Illuminate\Http\Response
     */
    public function destroy($cod_viajesNovios)
    {
        $viajeNo= ViajeNovio::findOrFail($cod_viajesNovios);
        $viajeNo->delete();
        return back();
    }
}
