<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotelytransporte extends Model
{

    protected $fillable=['nombre','descripcion','pension','tipoTransporte','numPersona','numHabitaciones','precio','rutaImg'];

    protected $primaryKey='cod_hotelytransporte';

    public $table="hotelytransporte";


    public function viajes() {
        return $this->belongTo(Viajes::class);
    }

    public function users() {
        return $this->belongTo(User::class);

    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }
}
