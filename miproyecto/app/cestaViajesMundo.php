<?php

namespace App;


class cestaViajesMundo
{
    public $objetos = null;
    public $cantidadTotal= 0;
    public $precioTotal= 0;

    public function __construct($cardVieja) {

        if($cardVieja) {
            $this->objetos=$cardVieja->objetos;
            $this->cantidadTotal=$cardVieja->cantidadTotal;
            $this->precioTotal=$cardVieja->precioTotal;
        }
    }//fin function

    public function añadir($objeto, $cod_viajes) {
        $almacenar=['cantidad'=> 0,'precio'=> $objeto->precio,'objeto'=>$objeto];

        if($this->objetos) {
            if(array_key_exists($cod_viajes,$this->objetos)) {
               $almacenar=$this->objetos[$cod_viajes];
            }
        }
        $almacenar['cantidad']++;
        $almacenar['precio'] = $objeto->precio * $almacenar['cantidad'];
        $this->objetos[$cod_viajes]=$almacenar;
        $this->cantidadTotal++;
        $this->precioTotal += $objeto->precio;

    } //fin function

    public function remover($cod_viajes) { //Quitar un elemento
        $this->objetos[$cod_viajes]['cantidad']--;
        $this->objetos[$cod_viajes]['precio']-= $this->objetos[$cod_viajes]['objeto']['precio'];
        $this->cantidadTotal--;
        $this->precioTotal-= $this->objetos[$cod_viajes]['objeto']['precio'];

        if($this->objetos[$cod_viajes]['cantidad'] <= 0) {
            unset($this->objetos[$cod_viajes]);
        }


    }//fin function



}
