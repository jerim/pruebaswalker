<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViajeNovio extends Model
{
    protected $fillable=['titulo','informacion','numPersona','origenYdestino','precio','rutaImg'];

    protected $primaryKey='cod_viajesNovios';

    public $table="viajes_novios";


    public function users() {
        return $this->belongsTo(User::class);
    }

    public function excursiones() {
        return $this->hasMany(Excursion::class);
    }

    public function hotelesytransportes() {
        return $this->belongsTo(Circuito::class);

    }

    public function circuitos() {
        return $this->belongTo(Hotelytransporte::class);
    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }
}
