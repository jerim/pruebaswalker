<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViajeTematico extends Model
{
    protected $fillable=['titulo','descripcion','numPersona','origenYdestino','precio'];

    protected $primaryKey='cod_viajesTematicos';

    public $table="viajes_tematicos";


    public function users() {
        return $this->belongsTo(User::class);
    }

    public function excursiones() {
        return $this->hasMany(Excursion::class);
    }

    public function hotelesytransportes() {
        return $this->belongsTo(Circuito::class);

    }

    public function circuitos() {
        return $this->belongTo(Hotelytransporte::class);
    }

    public function ofertas() {
        return $this->hasMany(Oferta::class);
    }
}
