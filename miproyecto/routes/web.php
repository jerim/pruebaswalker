<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*Rutas de Inicio-Home */
Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome', function () {
    return view('welcome');
});


/*Rutas del auth */
Auth::routes();
Route::get('/home','HomeController@index')->name('home');
Route::get('/logout','Auth\LoginController@logout');

/*Rutas del pie */
Route::get('/politicas', function () {
    return view('politicas');
});

Route::get('/contactos', function () {
    return view('contactos');
});

/*Rutas del Menu*/

/*Rutas del menu-Viajes */
Route::resource('/viajesMundo','ViajesController');
Route::resource('/viajesNovios','ViajesNoviosController');
Route::resource('/viajesTematicos','ViajesTematicosController');
Route::resource('/viajesInserso','ViajesInsersoController');

/*Rutas del menu-Excursion */
Route::resource('/excursionMundo','ExcursionController');
Route::resource('/excursionRecorrido','ExcursionRecorridoController');
Route::resource('/excursionCultural','ExcursionCulturalController');

/*Rutas del menu-Circuitos */
Route::resource('/circuitosItalia','CircuitosController');
Route::resource('/circuitosEmiratos','CircuitosEmiratosController');
Route::resource('/circuitosCorea','CircuitosCoreaController');
Route::resource('/circuitosNoruega','CircuitosNoruegaController');

/*Rutas del menu-Hoteles y Transporte */
Route::resource('/hotelesytransporteMundo','HotelesyTransportesMundoController');
Route::resource('/hotelesytransporteCiudades','HotelesyTransportesCiudadesController');

/*Rutas del menu-Ofertas */
Route::resource('/ofertasEscapada','OfertasEscapadaController');
Route::resource('/ofertasUltimaHora','OfertasUltimaHoraController');



/*Rutas del Show*/

/*Shows-viajes  */
Route::get('/viajesMundo/{cod_viajes}/imagenes/viajes/{rutaImg}', function ($cod_viajes) {
    return "Informacion de $cod_viajes";
});

Route::get('/viajesNovios/{cod_viajesNovios}/imagenes/viajesNovios/{rutaImg}', function ($cod_viajesNovios) {
    return "Informacion de $cod_viajesNovios";
});

Route::get('/viajesTematicos/{cod_viajesTematicos}',function ($cod_viajesTematicos){
    return "Informacion de $cod_viajesTematicos";
});

Route::get('/viajesInserso/{cod_viajesInsersos}/imagenes/viajesInserso/{rutaImg}', function ($cod_viajesInsersos) {
    return "Informacion de $cod_viajesInsersos";
});


/*Shows-Excursion  */
Route::get('/excursionMundo/{cod_excursion}',function ($cod_excursion){
    return "Informacion de $cod_excursion";
});

Route::get('/excursionRecorrido/{cod_excursionRecorrido}',function ($cod_excursionRecorrido){
    return "Informacion de $cod_excursion";
});

Route::get('/excursionCultural/{cod_excursionCultural}',function ($cod_excursionCultural){
    return "Informacion de $cod_excursion";
});


/*Shows-Circuitos*/
Route::get('/circuitosItalia/{cod_circuito}/imagenes/circuitosItalia/{rutaImg}', function ($cod_circuito) {
    return "Informacion de $cod_circuito";
});

Route::get('/circuitosEmiratos/{cod_circuitoEm}/imagenes/circuitosEmiratos/{rutaImg}', function ($cod_circuitoEm) {
    return "Informacion de $cod_circuitoEm";
});

Route::get('/circuitosCorea/{cod_circuitoCo}/imagenes/circuitosCorea/{rutaImg}', function ($cod_circuitoCo) {
    return "Informacion de $cod_circuitoCo";
});

Route::get('/circuitosNoruega/{cod_circuitoNo}/imagenes/circuitosNoruega/{rutaImg}', function ($cod_circuitoNo) {
    return "Informacion de $cod_circuitoNo";
});


/*Shows-Hoteles y transporte*/
Route::get('/hotelesytransporteMundo/{cod_hotelytransporte}',function ($cod_hotelytransporte){
    return "Informacion de $cod_hotelytransporte";
});

Route::get('/hotelesytransporteCiudades/{cod_hotelytransporteCi}',function ($cod_hotelytransporteCi){
    return "Informacion de $cod_hotelytransporteCi";
});


/*Shows-Ofertas*/
Route::get('/ofertasEscapada/{cod_oferta}/imagenes/ofertasEscapada/{rutaImg}',function ($cod_oferta){
    return "Informacion de $cod_oferta";
});

Route::get('/ofertasUltimaHora/{cod_ofertaUl}/imagenes/ofertasUltimaHora/{rutaImg}',function ($cod_ofertaUl){
    return "Informacion de $cod_ofertaUl";
});

/*Rutas de la cesta */

/*Rutas de la cesta de viajes */
Route::get('/añade/{cod_viajes}','ViajesController@añadirCesta');
Route::get('/cesta','ViajesController@comprobarCesta');
// Route::get('/remover/{cod_viajes}','ViajesController@removerUnElemento');
Route::get('/removerTodo','ViajesController@removerTodo');

Route::get('/removerUno/{cod_viajes}',
    ['uses'=> 'ViajesController@removerUnElemento',
     'as'=> 'viaje.removerUno']);

